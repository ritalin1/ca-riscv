# 「作って学ぶコンピュータアーキテクチャ」の内容で手を動かしてみた記録

## 作業環境

* OS - MacOS Catallina 10.15.7
* Hardware - Macbook Air Retina, 13-inch, 2020 (x86)

## 環境構築

### `第2章 RISC-Vの基礎知識`で使用する`gcc`

* https://github.com/riscv-admin/riscv-uefi-edk2-docs/tree/master/gcc-riscv-edk2-ci-toolchain よりダウンロード

### `第3章 LLVMの基礎知識`で使用する`llvm toolchain`

* 当初、MacOS(Catalina)でやっていこうと思っていたのだが、`LLVM`のビルドがどうやってもうまくいかないので、docker下のubuntuに切り替えた。
* 以下は、`MacOS`で頑張ってたときの残り香

↓↓↓↓ここから

* `homebrew`を介してインストール(`llvm@13`)

* 今回は`llvm`の各ツールに対して`homebrew link`は張らず、`llvm`ツールセットの展開先を`LLVM_HOME`として環境変数に入れて使用している

```
export LLVM_HOME=/usr/local/Cellar/llvm@13/13.0.1
$LLVM_HOME/bin/llc --version
```

* また`RISV-V`用の`libc`を参照する必要がある (が、どこから拾ってきたか記憶にない)
* これも`RISCV_LIBC`として環境変数に入れた

```
export RISCV_LIBC=/usr/local/Cellar/riscv-gnu-toolchain/master/riscv64-unknown-elf
```

↑↑↑↑ここまで

### LLVM開発用コンテナの準備

* `docker`イメージを準備する

```
docker run --name=riscv_dev_container -it ubuntu:latest

(in container)

apt-get update
apt-get -y dist-upgrade

apt-get install build-essential git -y
# LLVM gnu toolchainのビルドで必要
apt-get install -y autoconf automake autotools-dev curl python3 libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev

# LLVMのビルドで必要
apt install cmake ninja-build -y

exit

(in host)

docker commit riscv_dev_container riscv_dev_container 
```

* 作成した`docker`イメージを走らせ、`Risc-V`の`toolchain`と`LLVM`をビルドする

```
# コンテナを走らせる
docker run -v $PWD:/work -it riscv_dev_container

cd /work

#LLVM関連ソースを取得する

git clone --recursive --depth 1 https://github.com/riscv/riscv-gnu-toolchain
git clone --depth 1 https://github.com/llvm/llvm-project.git riscv-llvm

# ビルド成果物の展開先

mkdir _tools
export Build=/work/_tolls

# Toolchainのビルド
cd riscv-gnu-toolchain
./configure --prefix=`pwd`/../_tools --enable-multilib
make 

#Clangのビルド

cd iscv-llvm
ln -s ../../clang llvm/tools || true
mkdir _build

cmake \
-B ./_build \
-G Ninja -DCMAKE_BUILD_TYPE="Debug" \
-DLLVM_TARGETS_TO_BUILD="RISCV" \
-DLLVM_DEFAULT_TARGET_TRIPLE="riscv64-unknown-elf" \
-DBUILD_SHARED_LIBS=ON \
-DLLVM_OPTIMIZED_TABLEGEN=ON \
-DLLVM_USE_SPLIT_DWARF=ON \
-DLLVM_BUILD_TESTS=OFF \
-DLLVM_ENABLE_THREADS=OFF \
-DCMAKE_INSTALL_PREFIX="$PWD/../_tools" \
-DDEFAULT_SYSROOT="$PWD/../_tools/riscv64-unknown-elf" \
-S ./llvm

ninja -j`nproc` 
```

* `BUILD_SHARED_LIBS`を加えておかないと途中でエラーになった
* `DEFAULT_SYSROOT`を相対パスにしてしまうと、違うパスに解決され流ので注意
* `libclang-cpp.so`のLinkingでアホほどメモリを食うため、`docker`には11GBは与えておいたほうがいい
    * 10GBでは、`OOM`が発生して失敗した
* 現状、`runtime`でビルドエラーを起こす
    * `newlib`では`runtime`がビルドできないってどこかで見かけた
    * `gcc`でリンクすればワンチャン

```
#インストールまで進めないので、マニュアルで移動させる

ninja \
install-clang \
install-clang-libraries \
install-llvm-libraries \
install-llvm-headers \
install-clang-resource-headers \
install-llc
```

* ninja `install-clang-headers`が必要かどうか分からない

### サンプルコードの実行

* コンテナを走らせる

```
docker run -v $PWD:/work -v $PWD/../ca_riscv:/work/ca_riscv -it riscv_dev_container
```

* 書籍の`riscv_test.c`を[サポートページ](https://github.com/msyksphinz-self/support_ca_llvm_book)を反映させつつビルド
* `llc`の実行で、`float-abi=hard`なくても、実行まではこぎつけれた
    * 後々、失敗するのかもしれないけど・・・

```
${Build}/bin/clang riscv_test.c -emit-llvm -c --sysroot=${Build}/riscv64-unknown-elf
${Build}/bin/llc riscv_test.bc -march=riscv64 --float-abi=hard -mattr="+d,+f" -filetype=asm
${Build}/bin/riscv64-unknown-elf-gcc riscv_test.s -lc -o riscv_test
```

* `clang`で`*.s`まで一気通貫でビルドすることはできる
    * ポイントとしては、`march`と、`fno-addrsig`を指定すること
    * `fno-addrsig`なしだと、`.addrsig`疑似命令を生成してしまい`gcc`でのリンクで失敗する

```
${Build}/bin/clang -c riscv_test.c -S --sysroot=${Build}/riscv64-unknown-elf -fno-addrsig -march=rv64gc -o riscv_test.s
```