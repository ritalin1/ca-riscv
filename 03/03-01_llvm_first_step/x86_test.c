#include <stdio.h>

int main() {
    int total = 0;

    for (int i = 0; i <= 10; ++i) {
        total += i;
    }

    printf("Total = %d\n", total);

    return 0;
}

/*
(direct)
$Build/bin/clang x86_test.c -o x86_test_direct

(Sequential)
$Build/bin/clang -emit-llvm -c x86_test.c -o ./target/x86/x86_test.bc
$Build/bin/llc -filetype=asm ./target/x86/x86_test.bc
*/