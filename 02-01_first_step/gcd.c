#include <stdio.h>

int gcd(int a, int b) {
    if (a < b) {
        // swap
        int tmp = b;
        b = a;
        a = tmp;
    }

    int c;

    while (b != 0) {
        c = a % b;
        a = b;
        b = c;
    }

    return a;
}

int main() {
    printf("gcd(273, 21) = %d\n", gcd(273, 21));
    printf("gcd(411, 27117) = %d\n", gcd(411, 27117));

    return 0;
}